import numpy
from sklearn.linear_model import LogisticRegression

class GreenRoofClassifier(object):
    def __init__(self):
        self.logistic_classifier = LogisticRegression(n_jobs=-1)

    def fit(self, X_data, y):
        # Fit Logistic
        self.logistic_classifier.fit(X_data, y)
        pass

    def predict(self, numeric_data):
        numeric_data_predictions = self.logistic_classifier.predict(numeric_data)
        predictions = numeric_data_predictions
        return predictions

    def score(self, X_data, y):
        predictions = self.predict(X_data)
        correct_predictions = numpy.sum(predictions == y)

        return correct_predictions / len(y)
