import pandas
from sklearn.preprocessing import minmax_scale


class GreenRoofDataRepository():
    def retrieve_numeric_data(self):
        matrix = pandas.read_csv("data/train-features.csv").sort_values('ID').as_matrix()
        return minmax_scale(matrix)

    def retrieve_labels(self):
        return pandas.read_csv("data/data-id-train.csv").as_matrix()