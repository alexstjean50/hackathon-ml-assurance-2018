## Importing required Libraries
import itertools

import numpy
import os

import pandas as pd
import tensorflow as tf
from matplotlib import pyplot
from tensorflow.contrib.tensorboard.plugins import projector

PATH = os.getcwd()

## Path to save the embedding and checkpoints generated
LOG_DIR = PATH + '\\project-tensorboard\\log-1\\'

class DataVisualizer(object):
    def visualize_data(self):
        # Load Data
        train_data = pd.read_csv(PATH + "\\data\\train-features.csv", delimiter=",").sort_values(by=['ID'])
        labels = pd.read_csv(PATH + "\\data\\data-id-train-label-only.csv", delimiter=",", skiprows=[0]).as_matrix()
        ## Load the metadata file. Metadata consists your labels. This is optional. Metadata helps us visualize(color) different clusters that form t-SNE
        metadata = PATH + '\\data\\data-id-train-label-only.csv'

        tf_data = tf.Variable(train_data)
        ## Running TensorFlow Session
        with tf.Session() as sess:
            saver = tf.train.Saver([tf_data])
            sess.run(tf_data.initializer)
            saver.save(sess, os.path.join(LOG_DIR, 'tf_data.ckpt'))
            config = projector.ProjectorConfig()
            # One can add multiple embeddings.
            embedding = config.embeddings.add()
            embedding.tensor_name = tf_data.name
            # Link this tensor to its metadata(Labels) file
            embedding.metadata_path = metadata
            # Saves a config file that TensorBoard will read during startup.
            projector.visualize_embeddings(tf.summary.FileWriter(LOG_DIR), config)

    def plot_confusion_matrix(self, Y_test, predicted, possible_labels, normalize=False, title='Confusion matrix',
                              cmap=pyplot.cm.Blues):
        """
        This function prints and plots the confusion matrix.
        Normalization can be applied by setting `normalize=True`.
        """
        from sklearn.metrics import confusion_matrix
        confusion_mat = confusion_matrix(Y_test, predicted)
        classes = sorted(possible_labels)
        if normalize:
            confusion_mat = confusion_mat.astype('float') / confusion_mat.sum(axis=1)[:, numpy.newaxis]
            print("Normalized confusion matrix")
        else:
            print('Confusion matrix, without normalization')

        pyplot.figure(figsize=(20, 20))
        pyplot.imshow(confusion_mat, interpolation='nearest', cmap=cmap)
        pyplot.title(title)
        pyplot.colorbar()
        tick_marks = numpy.arange(len(classes))
        pyplot.xticks(tick_marks, classes, rotation=45)
        pyplot.yticks(tick_marks, classes)

        fmt = '.2f' if normalize else 'd'
        thresh = confusion_mat.max() / 2.
        for i, j in itertools.product(range(confusion_mat.shape[0]), range(confusion_mat.shape[1])):
            pyplot.text(j, i, format(confusion_mat[i, j], fmt),
                        horizontalalignment="center",
                        color="white" if confusion_mat[i, j] > thresh else "black")

        pyplot.ylabel('True label')
        pyplot.xlabel('Predicted label')
        pyplot.show()