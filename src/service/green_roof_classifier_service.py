from sklearn.model_selection import train_test_split

from ..classifier.green_roof_classifier import GreenRoofClassifier
from ..repository.green_root_data_repository import GreenRoofDataRepository

class GreenRoofClassifierService():
    def __init__(self):
        self._green_roof_data_repository = GreenRoofDataRepository()
        self._green_roof_classifier = GreenRoofClassifier()

    def retrieve_train_test_data(self):
        numeric_data = self._green_roof_data_repository.retrieve_numeric_data()
        data_labels = self._green_roof_data_repository.retrieve_labels()
        images_data = []

        train_data_ids, test_data_ids = train_test_split(data_labels[:, 0], test_size=0.2)

        train_data_ids = train_data_ids.astype(int) - 1
        test_data_ids = test_data_ids.astype(int) - 1

        train_numeric_data = numeric_data[train_data_ids]
        test_numeric_data = numeric_data[test_data_ids]
        train_labels = data_labels[:, 1][train_data_ids]
        test_labels = data_labels[:, 1][test_data_ids]

        return train_numeric_data, train_labels, test_numeric_data, test_labels, train_data_ids, test_data_ids



    def train(self, train_numeric_data, train_data_labels):
        self._green_roof_classifier.fit(train_numeric_data, train_data_labels)

    def predict(self, test_numeric_data):
        return self._green_roof_classifier.predict(test_numeric_data)

    def score(self, test_numeric_data, test_data_labels):
        score = self._green_roof_classifier.score(test_numeric_data, test_data_labels)
        return score