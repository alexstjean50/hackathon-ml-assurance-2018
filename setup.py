from src.service.green_roof_classifier_service import GreenRoofClassifierService
from src.exploration.data_visualisation import DataVisualizer
if __name__ == "__main__":
    green_roof_classifier_service = GreenRoofClassifierService()

    train_numeric_data, train_labels, test_numeric_data, test_labels, train_data_ids, test_data_ids = green_roof_classifier_service.retrieve_train_test_data()
    green_roof_classifier_service.train(train_numeric_data, train_labels)

    score = green_roof_classifier_service.score(test_numeric_data, test_labels)
    print("Accuracy w/ 80/20 split :" + str(score))
    predict = green_roof_classifier_service.predict(test_numeric_data)
    DataVisualizer().visualize_data()
    DataVisualizer().plot_confusion_matrix(test_labels, predict, set(test_labels))

