import os
import re

import pandas
## Import the random forest model.
import xgboost
from matplotlib import pyplot
from pip._internal.commands.list import tabulate
from sklearn.decomposition import PCA
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.neural_network import MLPClassifier
from sklearn.preprocessing import minmax_scale
from sklearn.svm import SVC
from xgboost import XGBClassifier

values = pandas.read_csv("data/train-features.csv").sort_values('ID')
data = values.as_matrix()
labels = pandas.read_csv("data/data-id-train.csv").as_matrix()

def get_best_features(data, labels):
    X_train, X_test, y_train , y_test = train_test_split(data, labels[:, 1], test_size=0.2)

    ## This line instantiates the model.
    rf = XGBClassifier(n_jobs=-1)
    ## Fit the model on your training data.
    rf.fit(X_train, y_train)
    ## And score it on your testing data.
    print(rf.score(X_test, y_test))

    feature_importances = pandas.DataFrame(rf.feature_importances_, index=list(values.columns), columns=['importance']).sort_values('importance', ascending=False)
    #list(values.columns)
    pyplot.show()
    with pandas.option_context('display.max_rows', None, 'display.max_columns', None):
        print(feature_importances)
        good_features = []
        for i, feature in enumerate(feature_importances.values):
            if feature[0] > 0:
                good_features.append(i)
        return good_features


def score_classif_with_best_features():
    best_features = get_best_features(data, labels)
    X_train, X_test, y_train, y_test = train_test_split(data[:, best_features], labels[:, 1], test_size=0.8)
    ## This line instantiates the model.
    classifiers = [
        LogisticRegression(n_jobs=-1),
        xgboost.XGBClassifier(n_jobs=-1)
    ]
    for clf in classifiers:
        ## Fit the model on your training data.
        clf.fit(X_train, y_train)
        ## And score it on your testing data.
        print(clf.score(X_test, y_test))

def score_classif():
    X_train, X_test, y_train, y_test = train_test_split(data, labels[:, 1], test_size=0.2)
    ## This line instantiates the model.
    classifiers = [
        LogisticRegression(n_jobs=-1),
        xgboost.XGBClassifier(n_jobs=-1)
    ]
    for clf in classifiers:
        ## Fit the model on your training data.
        clf.fit(X_train, y_train)
        ## And score it on your testing data.
        print(clf.score(X_test, y_test))

#score_classif()
score_classif_with_best_features()



